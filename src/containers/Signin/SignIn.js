import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody, MDBModalFooter, MDBIcon , MDBCardHeader } from 'mdbreact';


class signin extends React.Component {
  state = {
    open : "",
    error : ""
  }

  render() {
    const smallStyle = { fontSize: '0.8rem'}
    const {open , error} = this.state
    return (
      <MDBContainer>
        <br/><br/><br/>

        <MDBRow>
          <MDBCol md="9" lg="7" xl="5 " className="mx-auto mt-3">
            <MDBCard>

              {/* validation input */}
            <div className="alert alert-info" style={{display: open ? "" : "none"}}>
                      Inserted Successfully
              </div>
              <div className="alert alert-danger" style={{display: error ? "" : "none"}}>
                  {error}
            </div>

            <MDBCardBody className="mx-5">
              
                <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mb-3  ">
                 <h5><b>SIGN IN</b></h5>
                    </MDBCardHeader>
                <br/>

                <MDBInput label="Your email" group type="email" validate error="wrong" success="right"/>
                <MDBInput label="Your password" group type="password" validate containerClass="mb-0"/>
                <br/>
                <div className="text-center pt-3 mb-3">
                  <MDBBtn type="button" gradient="blue" rounded className="btn-block z-depth-1a">Sign in</MDBBtn>
                </div>
                <p className="dark-grey-text text-right d-flex justify-content-center mb-3 pt-2" style={smallStyle}> or Sign up with:</p>
                <div className="row my-3 d-flex justify-content-center">
                  <MDBBtn type="button" color="white" rounded className="mr-md-3 z-depth-1a"><MDBIcon fab icon="facebook-f" className="blue-text text-center" /></MDBBtn>
                  <MDBBtn type="button" color="white" rounded className="mr-md-3 z-depth-1a"><MDBIcon fab icon="twitter" className="blue-text" /></MDBBtn>
                  <MDBBtn type="button" color="white" rounded className="z-depth-1a"><MDBIcon fab icon="google-plus-g" className="blue-text" /></MDBBtn>
                </div>
              </MDBCardBody>

              <MDBModalFooter className="mx-5 pt-3 mb-1">
                <p className="grey-text d-flex justify-content-end" style={smallStyle}>Not a member ? <a href="#!" className="blue-text ml-1"> Contact to manager</a></p>
              </MDBModalFooter>

            </MDBCard>
          </MDBCol>
        </MDBRow>
        </MDBContainer>
    );
  }
}

export default signin;