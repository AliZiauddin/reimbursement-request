import React,{Component} from 'react';
import Tables from '../../components/Tables/Tables';
import NavBar from '../../components/NavBar/NavBar';
import {MDBContainer , MDBCard } from 'mdbreact'
import ReqForm from '../../components/Form/ReqForm'

class userhome extends Component{

  state = {
    columns :[
      {
        label: '#',
        field: 'id',
        sort: 'asc'
      },
      {
        label: 'Reason',
        field: 'reason',
        sort: 'asc'
      },
      {
        label: 'Comments',
        field: 'comments',
        sort: 'asc'
      },
      {
        label: 'Money',
        field: 'money',
        sort: 'asc'
      },
      {
        label: 'View',
        field: 'view',
        sort: 'asc'
      }
    ],

    rows : [
      {
        'id': 1,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 2,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 3,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 4,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 5,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 6,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 7,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 8,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 9,
        'reason': 'Need to update a computer Need to update a computer Need to update a computer Need to update a computer' ,
        'comments': 'Not fulfil our needs Need to update a computer Need to update a computer Need to update a computer Need to update a computer  ',
        'money': '$200',
        'view':<i href="/home" className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 10,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 11,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$400',
        'view':<i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      },
      {
        'id': 12,
        'reason': 'Need to update a computer',
        'comments': 'Not fulfil our needs',
        'money': '$200',
        'view': <i className="fa fa-eye-slash fa-2x text-center" aria-hidden="true"></i>
      }
    ],

    modal14: false,

    formVariables : {
      reason : "",
      comment :"",
      money : ""
    }
  }

  toggle = nr => () => {
    let modalNumber = 'modal' + nr
    this.setState({
    [modalNumber]: !this.state[modalNumber]
  });
    
  }

  handleChangeOnForm =()=>{

  }
 
  render ()
  {
    return(
      
      <MDBContainer fluid >

        <NavBar />
        <br/>
        <MDBContainer>
        <MDBCard>
        
        <Tables 
        Mainheading = {"REIMBURSEMENT REQUESTS"}
        formheading= {"SUBMIT REQUEST"}
        formRef = {"userform"}
        toggle = {this.toggle(14)}
        data = {this.state}
        />

         <ReqForm 
        toggle = {this.toggle(14)}
        modal14 = {this.state.modal14}
        MainHeading = {"SUBMIT REQUEST"}
        />

        </MDBCard>
        </MDBContainer>
      </MDBContainer>
    
    );
  }
  
}
export default userhome;