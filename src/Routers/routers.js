import React from 'react'
import {Route , Switch} from 'react-router-dom'
import userHome from '../containers/UserHome/UserHome'
import Signin from '../containers/Signin/SignIn'


const MainRouter = ()=>(
  
    <Switch>
      <Route exact path="/" component = {userHome}></Route>
      <Route exact path="/signin" component = {Signin}></Route>
    </Switch>

)

export default MainRouter;