import React from 'react';
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBInput, MDBModalFooter, MDBCardHeader } from 'mdbreact';


const Form = (props)=>{

  return (
      <MDBContainer>

        <MDBModal isOpen={props.modal14}  centered>
          <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3 ">
          <h4><b>{props.MainHeading}</b></h4>

          {/* div className="alert alert-danger" style={{display: error ? "" : "none"}}>
                  {error}
                </div>
                
                <div className="alert alert-info" style={{display: open ? "" : "none"}}>
                  Inserted Successfully
                </div> */}

          </MDBCardHeader>

          <MDBModalBody>

            <MDBInput 
                    label="Reason "  
                    group type="textarea" 
                    validate error="wrong"
                    success="right" 
                    rows="4"
                    onChange={props.reason}
                    value={props.reason}
                    />

                  <MDBInput 
                    label="Comment "
                    group type="textarea"
                    rows="4"
                    validate error="wrong"
                    success="right"
                    onChange={props.comment}
                    value={props.comment}
                    />

                  <MDBInput 
                    label="Money "
                    group type="text"
                    validate error="wrong"
                    success="right"
                    onChange={props.money}
                    value={props.money}
                    />
          </MDBModalBody>
          <MDBModalFooter>
            <br/>
            <MDBBtn color="secondary" onClick={props.toggle}>Close</MDBBtn>
            <MDBBtn color="primary">Save changes</MDBBtn>
          </MDBModalFooter>
        </MDBModal>
        
      </MDBContainer>
    );
  }

export default Form;